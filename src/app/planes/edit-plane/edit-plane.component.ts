﻿import { Component, OnInit, Output , EventEmitter } from '@angular/core';
import { PlanesService } from "../../planes/planes.service";
import {FormGroup , FormControl, FormBuilder} from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-edit-plane',
  templateUrl: './edit-plane.component.html',
  styleUrls: ['./edit-plane.component.css']
})
export class EditPlaneComponent implements OnInit {

@Output() updatePlane:EventEmitter<any> = new EventEmitter<any>(); 
@Output() updatePlanePs:EventEmitter<any> = new EventEmitter<any>();

manufacturer;
model;

service:PlanesService;

plane;

updateform = new FormGroup({
manufacturer:new FormControl(),
model:new FormControl()

});

  constructor(private route: ActivatedRoute ,service: PlanesService, private formBuilder: FormBuilder, private router: Router) { 
    this.service = service;
  }
          sendData() {
          this.updatePlane.emit(this.updateform.value.model);
         console.log(this.updateform.value);

          this.route.paramMap.subscribe(params=>{
            let id = params.get('id');
            this.service.putPlane(this.updateform.value, id).subscribe(
              response => {
               console.log(response.json());
                this.updatePlanePs.emit();
               this.router.navigate(['/']);
             }
           );
         })
       }
       
          ngOnInit() {
           this.route.paramMap.subscribe(params=>{
            let id = params.get('id');
             console.log(id);
            this.service.getPlane(id).subscribe(response=>{
               this.plane = response.json();
                console.log(this.plane);
                this.manufacturer = this.plane.manufacturer
                this.model = this.plane.model 
             })
           })
          }
}