﻿
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from "@angular/router";
import { PlanesService } from "./planes.service";

@Component({
  selector: 'app-planes',
  templateUrl: './planes.component.html',
  styleUrls: ['./planes.component.css']
})
export class PlanesComponent implements OnInit {

 optimisticAdd(plane){
   //console.log("addMessage work "+message); בדיקה
   var newKey = parseInt(this.planesKeys[this.planesKeys.length - 1],0) + 1;
    var newPlaneObject = {};
    newPlaneObject['manufacturer'] = plane; //גוף ההודעה עצמה
    this.planes[newKey] = newPlaneObject;
    this.planesKeys = Object.keys(this.planes);
  }

  //דרך פסימית להוספת משתמש
  pessimisticAdd(){
    this.service.getPlanes().subscribe(
      response=>{
        this.planes = response.json();
        this.planesKeys = Object.keys(this.planes);
    })
  }
  deletePlane(key){
    console.log.apply(key);
    let index = this.planesKeys.indexOf(key);
    this.planesKeys.splice(index,1); //אחד מבטא מחיקת רשומה אחת

    //delete from server
    this.service.deletePlane(key).subscribe(
      response=>console.log(response)
    );
  }

  planes;
  planesKeys;
  lengthKey;
  plane_service:PlanesService;
 searchForm = new FormGroup({
 name:new FormControl()
  });
  
  constructor(private service:PlanesService) {
 service.getPlanes().subscribe(
      response=>{
   this.planes = response.json();
        this.planesKeys = Object.keys(this.planes);
  });
  }




  ngOnInit() {
  }

}
