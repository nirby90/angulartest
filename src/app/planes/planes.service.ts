﻿import {Injectable } from '@angular/core';
import{Http,Headers} from '@angular/http';
import{HttpParams} from '@angular/common/http';
import {environment} from './../../environments/environment';
@Injectable()
export class PlanesService {
http:Http;


getPlanes(){
 return this.http.get('http://localhost/angular/slim/planes');
          }
    postPlanes(data){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('manufacturer',data.manufacturer).append('model',data.model);
    return this.http.post('http://localhost/angular/slim/planes', params.toString(),options);
  }
  
   deletePlane(key){
 return this.http.delete('http://localhost/angular/slim/planes'+ key);
}

  getPlane(id){
     return this.http.get('http://localhost/angular/slim/planes/'+ id);
  }

   putPlane(data,key){
     let options = {
       headers: new Headers({
       'content-type':'application/x-www-form-urlencoded'
       })
     }
     //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
     let params = new HttpParams().append('manufacturer',data.manufacturer).append('model',data.model);
   return this.http.put('http://localhost/angular/slim/planes/'+ key,params.toString(), options);
  }

   updatePlane(id,plane){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
      )};
    var params = new HttpParams().append('manufacturer',plane.manufacturer).append('model',plane.model);
    return this.http.put('http://localhost/angular/slim/planes/'+ id, params.toString(), options);      
   }

  
   
 
 constructor(http:Http){
   this.http = http;
   }
 
 
 }