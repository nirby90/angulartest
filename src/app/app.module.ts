﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { PlanesService } from "./planes/planes.service";


import { AppComponent } from './app.component';
import { PlanesComponent } from './planes/planes.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { NavigationComponent } from './navigation/navigation.component';;
import { EditPlaneComponent } from './planes/edit-plane/edit-plane.component'

@NgModule({
  declarations: [
    AppComponent,
    PlanesComponent,
    LoginComponent,
    NavigationComponent,
    EditPlaneComponent
,
    EditPlaneComponent  ],
  imports: [
    BrowserModule,
    HttpModule,
     FormsModule,
   ReactiveFormsModule,
    RouterModule.forRoot([
    {path: '', component: PlanesComponent},
  {path: 'planes', component: PlanesComponent},
  {path: 'editPlane/:id', component: EditPlaneComponent},
    {path: 'login', component: LoginComponent},
    {path: '**', component: PlanesComponent}
          ])
  ],
  providers: [PlanesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
